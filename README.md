# mth-report-mapping

This components displays plots about a mapping with single or multiple Mappings (maximum 4 at a time).

## Props

| Name        | Description                                             | Type     | Default |
| ----------- | ------------------------------------------------------- | -------- | ------- |
| `statsByMapping` | the statistics to display for each mapping. Maximum value : 4 different mappings. | `{ [mappingName: string]: MappingStats }` | —       |
| `mappingBy` | the type of biodata on which the mapping was performed | `Biodata` | —       |

## Types

| Name     | Value                                 |
| -------- | ------------------------------------------- |
| `Biodata` | `enum: { PATHWAY="pathways", METABOLITE="metabolites", REACTION="reactions", GENE="genes", PROTEIN="proteins", ENZYME="enzymes" }` |
| `Enrichment` | `{ label: string; nb_mapped_reactions: number; nb_total_reactions: number; nb_mapped_biodata: number; nb_total_biodata: number; pval: number; bonferroni_pval: number; bh_pval: number; pval_reactions: number; bonferroni_pval_reactions: number; bh_pval_reactions: number; }` |
| `MappingStats` | `{ nb_queries: number; nb_mapped_queries: number; nb_mapped_pathways: number; nb_total_pathways: number; nb_mapped_reactions: number; nb_total_reactions: number; nb_mapped_metabolites: number; nb_total_metabolites: number; nb_mapped_genes: number; nb_total_genes: number; nb_mapped_gene_products: number; nb_total_gene_products: number; nb_mapped_enzymes: number; nb_total_enzymes: number; nb_mapped_compartments: number; nb_total_compartments: number; enrichment: { [id: string]: Enrichment }; }` |

## Use example

This web component requires [Vuetify](https://vuetifyjs.com/en/).

```html
<template>
  <MthReportMapping :statsByMapping mappingBy="metabolites" />
</template>

<script setup lang="ts">
import { MthReportMapping } from "@metabohub/mth-report-mapping"; //import component
import "@metabohub/mth-report-mapping/dist/style.css"; // import style
</script>
```

## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Type-Check, Compile and Minify for Production

```sh
npm run build
```

### Run Unit Tests with [Vitest](https://vitest.dev/)

```sh
npm run test:unit
```

Check the coverage of the unit tests :
```sh
npm run coverage
```

### Lint with [ESLint](https://eslint.org/)

```sh
npm run lint
```

### View documentation

```sh
npm run storybook
```

## CICD pipeline

### Deploy

```yml
.publish:
  stage: deploy
  before_script:
    - apt-get update && apt-get install -y git default-jre
    - npm install
    - npm run build
```

This builds the component as an npm package.
