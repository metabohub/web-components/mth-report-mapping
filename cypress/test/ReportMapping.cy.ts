import ReportMapping from "@/components/ReportMapping.vue";
import { Biodata } from "@/types/Biodata";
import type { MappingStats } from "@/types/MappingStats";
import vuetify from "@/plugins/vuetify";

// DATA
const statsByMapping: { [key: string]: MappingStats } = {
  "Mapping 1": {
    nb_queries: 10,
    nb_mapped_queries: 9,
    nb_mapped_pathways: 3,
    nb_total_pathways: 14,
    nb_mapped_reactions: 63,
    nb_total_reactions: 254,
    nb_mapped_metabolites: 82,
    nb_total_metabolites: 204,
    nb_mapped_genes: 0,
    nb_total_genes: 0,
    nb_mapped_gene_products: 0,
    nb_total_gene_products: 0,
    nb_mapped_enzymes: 0,
    nb_total_enzymes: 0,
    nb_mapped_compartments: 0,
    nb_total_compartments: 0,
    enrichment: {
      p_T: {
        label: "transport",
        nb_mapped_reactions: 15,
        nb_total_reactions: 24,
        nb_mapped_metabolites: 24,
        nb_total_metabolites: 71,
        pval: 0.2,
        bonferroni_pval: 0.14,
        bh_pval: 0.34,
        pval_reactions: 0.31,
        bonferroni_pval_reactions: 0.25,
        bh_pval_reactions: 0.6,
      },
      p_D: {
        label: "deactivation",
        nb_mapped_reactions: 32,
        nb_total_reactions: 55,
        nb_mapped_metabolites: 67,
        nb_total_metabolites: 88,
        pval: 0.03,
        bonferroni_pval: 0.06,
        bh_pval: 0.042,
        pval_reactions: 0.1,
        bonferroni_pval_reactions: 0.09,
        bh_pval_reactions: 0.03,
      },
      p_A: {
        label: "degration and bioactivation of molecule AAAAA",
        nb_mapped_reactions: 47,
        nb_total_reactions: 101,
        nb_mapped_metabolites: 28,
        nb_total_metabolites: 45,
        pval: 0.08,
        bonferroni_pval: 0.1,
        bh_pval: 0.34,
        pval_reactions: 0.2,
        bonferroni_pval_reactions: 0.31,
        bh_pval_reactions: 0.45,
      },
    },
  },
  "Mapping 2": {
    nb_queries: 5,
    nb_mapped_queries: 2,
    nb_mapped_pathways: 2,
    nb_total_pathways: 14,
    nb_mapped_reactions: 41,
    nb_total_reactions: 254,
    nb_mapped_metabolites: 53,
    nb_total_metabolites: 204,
    nb_mapped_genes: 0,
    nb_total_genes: 0,
    nb_mapped_gene_products: 0,
    nb_total_gene_products: 0,
    nb_mapped_enzymes: 0,
    nb_total_enzymes: 0,
    nb_mapped_compartments: 0,
    nb_total_compartments: 0,
    enrichment: {
      p_T: {
        label: "transport",
        nb_mapped_reactions: 10,
        nb_total_reactions: 24,
        nb_mapped_metabolites: 14,
        nb_total_metabolites: 71,
        pval: 0.04,
        bonferroni_pval: 0.28,
        bh_pval: 0.014,
        pval_reactions: 0.65,
        bonferroni_pval_reactions: 0.2,
        bh_pval_reactions: 0.08,
      },
      p_A: {
        label: "degration and bioactivation of molecule AAAAA",
        nb_mapped_reactions: 24,
        nb_total_reactions: 101,
        nb_mapped_metabolites: 30,
        nb_total_metabolites: 45,
        pval: 0.19,
        bonferroni_pval: 0.45,
        bh_pval: 0.03,
        pval_reactions: 0.05,
        bonferroni_pval_reactions: 0.1,
        bh_pval_reactions: 0.3,
      },
    },
  },
};

describe("<ReportMapping />", () => {
  beforeEach(() => {
    cy.viewport(1200, 800);
  });

  it("renders", () => {
    cy.mount(ReportMapping, {
      global: {
        plugins: [vuetify],
      },
      props: {
        statsByMapping,
        mappingBy: Biodata.METABOLITE,
      },
    });

    // display tabs
    cy.get(".v-tabs").should("exist");
    // display a plot
    cy.get(".echarts").should("exist");
    // display buttons
    cy.get(".v-btn-group").should("exist");
  });

  it("tabs display different windows", () => {
    cy.mount(ReportMapping, {
      global: {
        plugins: [vuetify],
      },
      props: {
        statsByMapping,
      },
    });

    // select
    cy.get("[data-test='SelectTab']").click();
    cy.get(".v-selection-control").should("exist");
    cy.get(".echarts").should("not.exist");
    cy.get(".v-btn-group").should("not.exist");

    // pathways
    cy.get("[data-test='PathwaysTab']").click();
    cy.get(".echarts").should("exist");
    cy.get(".v-btn-group").should("exist");

    // reactions
    cy.get("[data-test='ReactionsTab']").click();
    cy.get(".echarts").should("exist");
    cy.get(".v-btn-group").should("exist");
  });

  it("doesn't display graphs when no queries mapped", () => {
    const statsNoMapping = {
      "Mapping 1": {
        nb_queries: 10,
        nb_mapped_queries: 0,
        nb_mapped_pathways: 0,
        nb_total_pathways: 14,
        nb_mapped_reactions: 0,
        nb_total_reactions: 254,
        nb_mapped_metabolites: 0,
        nb_total_metabolites: 204,
        nb_mapped_genes: 0,
        nb_total_genes: 0,
        nb_mapped_gene_products: 0,
        nb_total_gene_products: 0,
        nb_mapped_enzymes: 0,
        nb_total_enzymes: 0,
        nb_mapped_compartments: 0,
        nb_total_compartments: 0,
        enrichment: {},
      },
    };
    cy.mount(ReportMapping, {
      global: {
        plugins: [vuetify],
      },
      props: {
        statsByMapping: statsNoMapping,
        mappingBy: Biodata.METABOLITE,
      },
    });

    cy.get(".echarts").should("not.exist");
    cy.get(".v-btn-group").should("not.exist");
  });
});
