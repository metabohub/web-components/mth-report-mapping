import SelectPathways from "@/components/SelectPathways.vue";
import { Biodata } from "@/types/Biodata";
import type { MappingStats } from "@/types/MappingStats";
import type { PathwayInfos } from "@/types/PathwayInfos";
import { enrichmentToPathwayInfos } from "@/composables/Util";
import vuetify from "@/plugins/vuetify";

// DATA
const stats: MappingStats = {
  map1: {
    nb_queries: 5,
    nb_mapped_queries: 2,
    nb_mapped_pathways: 1,
    nb_total_pathways: 3,
    nb_mapped_reactions: 3,
    nb_total_reactions: 5,
    nb_mapped_metabolites: 4,
    nb_total_metabolites: 7,
    nb_mapped_genes: 1,
    nb_total_genes: 3,
    nb_mapped_gene_products: 1,
    nb_total_gene_products: 3,
    nb_mapped_enzymes: 1,
    nb_total_enzymes: 3,
    nb_mapped_compartments: 1,
    nb_total_compartments: 2,
    enrichment: {
      pathway1: {
        label: "pathway1",
        nb_mapped_reactions: 3,
        nb_total_reactions: 5,
        nb_mapped_biodata: 4,
        nb_total_biodata: 7,
        pval: 0.001,
        bonferroni_pval: 0.005,
        bh_pval: 0.003,
        pval_reactions: 0.01,
        bonferroni_pval_reactions: 0.05,
        bh_pval_reactions: 0.03,
      },
      pathway2: {
        label: "pathway2",
        nb_mapped_reactions: 3,
        nb_total_reactions: 5,
        nb_mapped_biodata: 4,
        nb_total_biodata: 7,
        pval: 0.001,
        bonferroni_pval: 0.005,
        bh_pval: 0.003,
        pval_reactions: 0.01,
        bonferroni_pval_reactions: 0.05,
        bh_pval_reactions: 0.03,
      },
      pathway3: {
        label: "pathway3",
        nb_mapped_reactions: 3,
        nb_total_reactions: 5,
        nb_mapped_biodata: 4,
        nb_total_biodata: 7,
        pval: 0.001,
        bonferroni_pval: 0.005,
        bh_pval: 0.003,
        pval_reactions: 0.01,
        bonferroni_pval_reactions: 0.05,
        bh_pval_reactions: 0.03,
      },
      pathway4: {
        label: "pathway4",
        nb_mapped_reactions: 3,
        nb_total_reactions: 5,
        nb_mapped_biodata: 4,
        nb_total_biodata: 7,
        pval: 0.001,
        bonferroni_pval: 0.005,
        bh_pval: 0.003,
        pval_reactions: 0.01,
        bonferroni_pval_reactions: 0.05,
        bh_pval_reactions: 0.03,
      },
    },
  },
};
const mappedPathways: PathwayInfos[] = enrichmentToPathwayInfos(stats);

describe("<SelectPathways />", () => {
  beforeEach(() => {
    cy.viewport(1200, 800);
  });

  it("should select and unselect all", () => {
    cy.mount(SelectPathways, {
      global: {
        plugins: [vuetify],
      },
      props: {
        statsByMapping: stats,
        mappingBy: Biodata.METABOLITE,
        modelValue: mappedPathways,
      },
    });

    // init : every pathway should be selected
    cy.get('input[type="checkbox"]').should(
      "have.length",
      mappedPathways.length + 1,
    ); // +1 for the select all checkbox
    cy.get(".mdi-checkbox-marked").should(
      "have.length",
      mappedPathways.length + 1,
    );
    cy.get(".mdi-checkbox-blank-outline").should("have.length", 0);

    // click on select all
    cy.get('input[type="checkbox"]').first().click();

    // everything should be unselected
    cy.get(".mdi-checkbox-marked").should("have.length", 0);
    cy.get(".mdi-checkbox-blank-outline").should(
      "have.length",
      mappedPathways.length + 1,
    );
  });
});
