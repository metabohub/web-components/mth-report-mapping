import { describe, expect, test } from "vitest";
import { mount } from "@vue/test-utils";
import { createVuetify } from "vuetify";
import SelectPathways from "../SelectPathways.vue";
import { Biodata } from "@/types/Biodata";
import type { MappingStats } from "@/types/MappingStats";
import type { PathwayInfos } from "@/types/PathwayInfos";
import { enrichmentToPathwayInfos } from "@/composables/Util";

function selectPathwaysWrapper(
  statsByMapping: { [Mapping: string]: MappingStats },
  mappingBy: Biodata,
  mappedPathways: PathwayInfos[],
) {
  const vuetify = createVuetify();
  return mount(SelectPathways, {
    global: {
      plugins: [vuetify],
    },
    props: {
      statsByMapping,
      mappingBy,
      modelValue: mappedPathways,
    },
  });
}

describe("SelectPathways.vue", () => {
  test("should render", () => {
    const stats = {
      map1: {
        nb_queries: 5,
        nb_mapped_queries: 2,
        nb_mapped_pathways: 1,
        nb_total_pathways: 3,
        nb_mapped_reactions: 3,
        nb_total_reactions: 5,
        nb_mapped_metabolites: 4,
        nb_total_metabolites: 7,
        nb_mapped_genes: 1,
        nb_total_genes: 3,
        nb_mapped_gene_products: 1,
        nb_total_gene_products: 3,
        nb_mapped_enzymes: 1,
        nb_total_enzymes: 3,
        nb_mapped_compartments: 1,
        nb_total_compartments: 2,
        enrichment: {
          pathway1: {
            label: "pathway1",
            nb_mapped_reactions: 3,
            nb_total_reactions: 5,
            nb_mapped_biodata: 4,
            nb_total_biodata: 7,
            pval: 0.001,
            bonferroni_pval: 0.005,
            bh_pval: 0.003,
            pval_reactions: 0.01,
            bonferroni_pval_reactions: 0.05,
            bh_pval_reactions: 0.03,
          },
        },
      },
    };
    const mappedPathways = enrichmentToPathwayInfos(stats);
    const wrapper = selectPathwaysWrapper(
      stats,
      Biodata.METABOLITE,
      mappedPathways,
    );

    // should display the list of pathways
    expect(wrapper.findAll('[data-test="mappedPathwayRow"]').length).toBe(
      mappedPathways.length,
    );
  });
});
