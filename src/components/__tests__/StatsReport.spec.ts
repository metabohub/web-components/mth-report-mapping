import { describe, expect, test } from "vitest";
import { mount } from "@vue/test-utils";
import { createVuetify } from "vuetify";
import StatsReport from "../StatsReport.vue";
import { Biodata } from "@/types/Biodata";
import type { MappingStats } from "@/types/MappingStats";

function statsReportWrapper(
  statsByMapping: { [Mapping: string]: MappingStats },
  mappingBy: Biodata,
) {
  const vuetify = createVuetify();
  return mount(StatsReport, {
    global: {
      plugins: [vuetify],
    },
    props: {
      statsByMapping,
      mappingBy,
    },
  });
}

describe("StatsReport.vue", () => {
  test("should render", () => {
    const stats = {
      map1: {
        nb_queries: 5,
        nb_mapped_queries: 2,
        nb_mapped_pathways: 1,
        nb_total_pathways: 3,
        nb_mapped_reactions: 3,
        nb_total_reactions: 5,
        nb_mapped_metabolites: 4,
        nb_total_metabolites: 7,
        nb_mapped_genes: 1,
        nb_total_genes: 3,
        nb_mapped_gene_products: 1,
        nb_total_gene_products: 3,
        nb_mapped_enzymes: 1,
        nb_total_enzymes: 3,
        nb_mapped_compartments: 1,
        nb_total_compartments: 2,
        enrichment: {
          pathway1: {
            label: "pathway1",
            nb_mapped_reactions: 3,
            nb_total_reactions: 5,
            nb_mapped_biodata: 4,
            nb_total_biodata: 7,
            pval: 0.001,
            bonferroni_pval: 0.005,
            bh_pval: 0.003,
            pval_reactions: 0.01,
            bonferroni_pval_reactions: 0.05,
            bh_pval_reactions: 0.03,
          },
        },
      },
    };
    const wrapper = statsReportWrapper(stats, Biodata.METABOLITE);

    // should display 1 column
    expect(wrapper.findAll(".v-col").length).toBe(1);
    // display name of mapping
    expect(wrapper.find("[data-test='title']").exists()).toBeTruthy();
    expect(wrapper.find("[data-test='title']").text()).toBe("map1");
    // should display stats
    expect(wrapper.find("[data-test='nbTotalQueries']").exists()).toBeTruthy();
    expect(wrapper.find("[data-test='nbTotalQueries']").text()).toContain(
      stats["map1"].nb_queries,
    );
    expect(wrapper.find("[data-test='nbQueries']").exists()).toBeTruthy();
    expect(wrapper.find("[data-test='nbQueries']").text()).toContain(
      stats["map1"].nb_mapped_queries,
    );
    expect(
      wrapper.find("[data-test='nbMappedBiodatas']").exists(),
    ).toBeTruthy();
    expect(wrapper.find("[data-test='nbMappedBiodatas']").text()).toContain(
      stats["map1"].nb_mapped_metabolites,
    );
    expect(
      wrapper.find("[data-test='nbMappedPathways']").exists(),
    ).toBeTruthy();
    expect(wrapper.find("[data-test='nbMappedPathways']").text()).toContain(
      stats["map1"].nb_mapped_pathways,
    );
  });
});
