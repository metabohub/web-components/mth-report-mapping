import { describe, expect, test } from "vitest";
import {
  pathwayInfosToBarChartSeries,
  pathwayInfosToScatterChartSeries,
} from "@/composables/Util";
import type { PathwayInfos } from "@/types/PathwayInfos";

const pathways: PathwayInfos[] = [
  {
    select: true,
    label: "Glycolysis / Gluconeogenesis",
    reactionTotal: 20,
    biodataTotal: 30,
    mappings: {
      Mapping1: {
        reactionMapped: 10,
        biodataMapped: 20,
        pval: 0.05,
        bonferroni_pval: 0.15,
        bh_pval: 0.2,
        pval_reactions: 0.1,
        bonferroni_pval_reactions: 0.25,
        bh_pval_reactions: 0.3,
      },
    },
  },
  {
    select: true,
    label: "Citrate cycle (TCA cycle)",
    reactionTotal: 25,
    biodataTotal: 35,
    mappings: {
      Mapping1: {
        reactionMapped: 15,
        biodataMapped: 25,
        pval: 0.1,
        bonferroni_pval: 0.25,
        bh_pval: 0.3,
        pval_reactions: 0.15,
        bonferroni_pval_reactions: 0.35,
        bh_pval_reactions: 0.4,
      },
    },
  },
  {
    select: true,
    label: "Pentose phosphate pathway",
    reactionTotal: 30,
    biodataTotal: 40,
    mappings: {
      Mapping1: {
        reactionMapped: 20,
        biodataMapped: 30,
        pval: 0.15,
        bonferroni_pval: 0.35,
        bh_pval: 0.4,
        pval_reactions: 0.2,
        bonferroni_pval_reactions: 0.45,
        bh_pval_reactions: 0.5,
      },
    },
  },
];

describe("pathwayInfosToBarChartSeries", () => {
  test("REACTIONS and absolute values", () => {
    expect(pathwayInfosToBarChartSeries(pathways, "REACTIONS", "abs")).toEqual([
      {
        data: [10, 15, 20],
        name: "Mapping1",
        type: "bar",
      },
      {
        color: "rgba(158, 159, 163, 0.5)",
        data: [20, 25, 30],
        name: "Total",
        type: "bar",
      },
    ]);
  });
  test("REACTIONS and percentage values", () => {
    expect(
      pathwayInfosToBarChartSeries(pathways, "REACTIONS", "percent"),
    ).toEqual([
      {
        data: [50, 60, 66.66666666666667],
        name: "Mapping1",
        type: "bar",
      },
    ]);
  });
  test("PATHWAYS and absolute values", () => {
    expect(pathwayInfosToBarChartSeries(pathways, "PATHWAYS", "abs")).toEqual([
      {
        data: [20, 25, 30],
        name: "Mapping1",
        type: "bar",
      },
      {
        color: "rgba(158, 159, 163, 0.5)",
        data: [30, 35, 40],
        name: "Total",
        type: "bar",
      },
    ]);
  });
  test("PATHWAYS and percentage values", () => {
    expect(
      pathwayInfosToBarChartSeries(pathways, "PATHWAYS", "percent"),
    ).toEqual([
      {
        data: [66.66666666666667, 71.42857142857143, 75],
        name: "Mapping1",
        type: "bar",
      },
    ]);
  });
});

describe("pathwayInfosToScatterChartSeries", () => {
  test("REACTIONS and percentage values", () => {
    const result = pathwayInfosToScatterChartSeries(
      pathways,
      "REACTIONS",
      "percent",
    );
    expect(result[0].data).toEqual([
      [50, 0, 10, "Glycolysis / Gluconeogenesis"],
      [60, 1, 15, "Citrate cycle (TCA cycle)"],
      [66.66666666666667, 2, 20, "Pentose phosphate pathway"],
    ]);
  });
  test("REACTIONS and p values", () => {
    const result = pathwayInfosToScatterChartSeries(
      pathways,
      "REACTIONS",
      "pvalue",
    );
    expect(result[0].data).toEqual([
      [0.1, 0, 10, "Glycolysis / Gluconeogenesis"],
      [0.15, 1, 15, "Citrate cycle (TCA cycle)"],
      [0.2, 2, 20, "Pentose phosphate pathway"],
    ]);
  });
  test("REACTIONS and bonferroni p values", () => {
    const result = pathwayInfosToScatterChartSeries(
      pathways,
      "REACTIONS",
      "bonferroni",
    );
    expect(result[0].data).toEqual([
      [0.25, 0, 10, "Glycolysis / Gluconeogenesis"],
      [0.35, 1, 15, "Citrate cycle (TCA cycle)"],
      [0.45, 2, 20, "Pentose phosphate pathway"],
    ]);
  });
  test("REACTIONS and benjamini hochsberg p values", () => {
    const result = pathwayInfosToScatterChartSeries(
      pathways,
      "REACTIONS",
      "bh",
    );
    expect(result[0].data).toEqual([
      [0.3, 0, 10, "Glycolysis / Gluconeogenesis"],
      [0.4, 1, 15, "Citrate cycle (TCA cycle)"],
      [0.5, 2, 20, "Pentose phosphate pathway"],
    ]);
  });
});
