import { describe, expect, test } from "vitest";
import { transformInterval } from "@/composables/Util";

describe("trasnformInterval", () => {
  test("should shift a value to a different range", () => {
    expect(transformInterval(1, 0, 10)).toBeCloseTo(6);
    expect(transformInterval(5, -10, 20)).toBeCloseTo(10);
    expect(transformInterval(30, 0, 100)).toBeCloseTo(8);
  });
});
