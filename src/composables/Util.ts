import { MappingStats } from "@/types/MappingStats";
import type { PathwayInfos } from "@/types/PathwayInfos";
import type { SeriesOption } from "echarts";

function formatLabel(label: string): string {
  label = label.replaceAll("less_than", "<");
  label = label.replaceAll("greater_than", ">");
  return label;
}

export function enrichmentToPathwayInfos(statsByMapping: {
  [mapping: string]: MappingStats;
}): PathwayInfos[] {
  const mappingNames: string[] = Object.keys(statsByMapping);
  const pathways = {};

  Object.entries(statsByMapping).forEach(([currentMapping, stats]) => {
    // fill the pathways object with every pathway
    Object.entries(stats.enrichment).forEach(([pathway, enrichment]) => {
      // the pathway is not yet in the object
      if (!pathways[pathway]) {
        // create the pathway object
        pathways[pathway] = {
          select: true,
          label: formatLabel(enrichment.label),
          reactionTotal: enrichment.nb_total_reactions,
          biodataTotal: enrichment.nb_total_biodata,
          mappings: {},
        };
        // add every mapping with default stats
        mappingNames.forEach((name) => {
          pathways[pathway].mappings[name] = {
            reactionMapped: 0,
            biodataMapped: 0,
            pval: 1,
            bonferroni_pval: 1,
            bh_pval: 1,
            pval_reactions: 1,
            bonferroni_pval_reactions: 1,
            bh_pval_reactions: 1,
          };
        });
      }
      // add the stats for the current mapping
      pathways[pathway].mappings[currentMapping] = {
        reactionMapped: enrichment.nb_mapped_reactions,
        biodataMapped: enrichment.nb_mapped_biodata,
        pval: enrichment.pval,
        bonferroni_pval: enrichment.bonferroni_pval,
        bh_pval: enrichment.bh_pval,
        pval_reactions: enrichment.pval_reactions,
        bonferroni_pval_reactions: enrichment.bonferroni_pval_reactions,
        bh_pval_reactions: enrichment.bh_pval_reactions,
      };
      //
    });
  });

  return Object.values(pathways);
}

/**
 * Transforms a value from one interval to another.
 * This function works by first calculating the scale factor between the two intervals,
 * and then the shift necessary to move the current interval to the target interval.
 * It then applies these transformations to the input number.
 * @param input The value to be transformed.
 * @param currentMin The minimum value of the current interval.
 * @param currentMax The maximum value of the current interval.
 * @returns The transformed value.
 */
export function transformInterval(
  input: number,
  currentMin: number,
  currentMax: number,
) {
  // desired size of the bubble : between 5 and 15
  const targetMax: number = 15;
  const targetMin: number = 5;

  const scale: number = (targetMax - targetMin) / (currentMax - currentMin);
  const shift: number = targetMin - currentMin * scale;

  return input * scale + shift;
}

export function pathwayInfosToBarChartSeries(
  pathways: PathwayInfos[],
  choiceEnrichment: "REACTIONS" | "PATHWAYS",
  valueType: "abs" | "percent",
): SeriesOption[] {
  const series: SeriesOption[] = [];

  const mappings: string[] = Object.keys(pathways[0].mappings);

  if (valueType == "abs") {
    // Absolute values : a column per mapping + a greyed column for the total
    // mappings
    mappings.forEach((mapping: string) => {
      series.push({
        type: "bar",
        name: mapping,
        data: pathways.map((p: PathwayInfos) =>
          choiceEnrichment == "REACTIONS"
            ? p.mappings[mapping].reactionMapped
            : p.mappings[mapping].biodataMapped,
        ),
      });
    });
    // total
    series.push({
      type: "bar",
      color: "rgba(158, 159, 163, 0.5)",
      name: "Total",
      data: pathways.map((l: PathwayInfos) =>
        choiceEnrichment == "REACTIONS" ? l.reactionTotal : l.biodataTotal,
      ),
    });
  } else {
    // Percentage values : a column per mapping
    mappings.forEach((mapping: string) => {
      series.push({
        type: "bar",
        name: mapping,
        data: pathways.map((p: PathwayInfos) => {
          return choiceEnrichment == "REACTIONS"
            ? (100 * p.mappings[mapping].reactionMapped) / p.reactionTotal
            : (100 * p.mappings[mapping].biodataMapped) / p.biodataTotal;
        }),
      });
    });
  }

  return series;
}

export function pathwayInfosToScatterChartSeries(
  pathways: PathwayInfos[],
  choiceEnrichment: "REACTIONS" | "PATHWAYS",
  valueType: "pvalue" | "percent" | "bonferroni" | "bh",
): SeriesOption[] {
  const series: SeriesOption[] = [];

  const mappings: string[] = Object.keys(pathways[0].mappings);

  // function to compute the size of the bubble
  const getSymbolSize = (mapping: string, z: number): number => {
    return choiceEnrichment == "REACTIONS"
      ? transformInterval(
          z,
          Math.min(
            ...pathways.map(
              (p: PathwayInfos) => p.mappings[mapping].reactionMapped,
            ),
          ),
          Math.max(
            ...pathways.map(
              (p: PathwayInfos) => p.mappings[mapping].reactionMapped,
            ),
          ),
        )
      : transformInterval(
          z,
          Math.min(
            ...pathways.map(
              (p: PathwayInfos) => p.mappings[mapping].biodataMapped,
            ),
          ),
          Math.max(
            ...pathways.map(
              (p: PathwayInfos) => p.mappings[mapping].reactionMapped,
            ),
          ),
        );
  };
  const thresholdLineSeries: SeriesOption = {
    name: "Threshold",
    type: "line",
    data: [
      [0.05, -1],
      [0.05, pathways.length],
    ],
    lineStyle: {
      color: "#EB4441",
      type: "dashed",
    },
    showSymbol: false,
  };
  // data is the only variable changing
  const scatterSeries = (
    mapping: string,
    data: Array<Array<string | number>>,
  ): SeriesOption => {
    return {
      type: "scatter",
      name: mapping,
      data: data,
      // size of bubble : z
      symbolSize: function (data: Array<string | number>) {
        return getSymbolSize(mapping, data[2] as number);
      },
    };
  };

  if (valueType == "percent") {
    // Percentage of biodata mapped compared to total
    mappings.forEach((mapping: string) => {
      series.push(
        scatterSeries(
          mapping,
          pathways.map((p: PathwayInfos, index: number) => {
            return [
              // x : percentage of reactions with biodatas mapped / of biodatas mapped
              choiceEnrichment == "REACTIONS"
                ? (100 * p.mappings[mapping].reactionMapped) / p.reactionTotal
                : (100 * p.mappings[mapping].biodataMapped) / p.biodataTotal,
              // y : pathway
              index,
              // z : number of reactions / biodatas
              choiceEnrichment == "REACTIONS"
                ? p.mappings[mapping].reactionMapped
                : p.mappings[mapping].biodataMapped,
              // label
              p.label,
            ];
          }),
        ),
      );
    });
  } else if (valueType == "pvalue") {
    // p value + a vertical line for the threshold
    mappings.forEach((mapping: string) => {
      series.push(
        scatterSeries(
          mapping,
          pathways.map((p: PathwayInfos, index: number) => {
            return [
              // x : p value
              choiceEnrichment == "REACTIONS"
                ? p.mappings[mapping].pval_reactions
                : p.mappings[mapping].pval,
              // y : pathway
              index,
              // z : number of reactions / biodatas
              choiceEnrichment == "REACTIONS"
                ? p.mappings[mapping].reactionMapped
                : p.mappings[mapping].biodataMapped,
              // label
              p.label,
            ];
          }),
        ),
      );
    });
    // threshold
    series.push(thresholdLineSeries);
  } else if (valueType == "bonferroni") {
    // bonferroni p value + a vertical line for the threshold
    mappings.forEach((mapping: string) => {
      series.push(
        scatterSeries(
          mapping,
          pathways.map((p: PathwayInfos, index: number) => {
            return [
              // x : bonferroni p value
              choiceEnrichment == "REACTIONS"
                ? p.mappings[mapping].bonferroni_pval_reactions
                : p.mappings[mapping].bonferroni_pval,
              // y : pathway
              index,
              // z : number of reactions / biodatas
              choiceEnrichment == "REACTIONS"
                ? p.mappings[mapping].reactionMapped
                : p.mappings[mapping].biodataMapped,
              // label
              p.label,
            ];
          }),
        ),
      );
    });
    // threshold
    series.push(thresholdLineSeries);
  } else {
    // benjamini hochsberg p value + a vertical line for the threshold
    mappings.forEach((mapping: string) => {
      series.push(
        scatterSeries(
          mapping,
          pathways.map((p: PathwayInfos, index: number) => {
            return [
              // x : benjamini hochsberg p value
              choiceEnrichment == "REACTIONS"
                ? p.mappings[mapping].bh_pval_reactions
                : p.mappings[mapping].bh_pval,
              // y : pathway
              index,
              // z : number of reactions / biodatas
              choiceEnrichment == "REACTIONS"
                ? p.mappings[mapping].reactionMapped
                : p.mappings[mapping].biodataMapped,
              // label
              p.label,
            ];
          }),
        ),
      );
    });
    // threshold
    series.push(thresholdLineSeries);
  }

  return series;
}
