import type { Enrichment } from "./Enrichment";

export interface MappingStats {
  nb_queries: number;
  nb_mapped_queries: number;
  nb_mapped_pathways: number;
  nb_total_pathways: number;
  nb_mapped_reactions: number;
  nb_total_reactions: number;
  nb_mapped_metabolites: number;
  nb_total_metabolites: number;
  nb_mapped_genes: number;
  nb_total_genes: number;
  nb_mapped_gene_products: number;
  nb_total_gene_products: number;
  nb_mapped_enzymes: number;
  nb_total_enzymes: number;
  nb_mapped_compartments: number;
  nb_total_compartments: number;
  enrichment: { [id: string]: Enrichment };
}
