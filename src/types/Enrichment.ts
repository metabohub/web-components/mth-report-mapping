export interface Enrichment {
  label: string;
  nb_mapped_reactions: number;
  nb_total_reactions: number;
  nb_mapped_biodata: number;
  nb_total_biodata: number;
  pval: number;
  bonferroni_pval: number;
  bh_pval: number;
  pval_reactions: number;
  bonferroni_pval_reactions: number;
  bh_pval_reactions: number;
}
