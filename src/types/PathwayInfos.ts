export interface PathwayInfos {
  select: boolean;
  label: string;
  reactionTotal: number;
  biodataTotal: number;
  mappings: {
    [name: string]: {
      reactionMapped: number;
      biodataMapped: number;
      pval: number;
      bonferroni_pval: number;
      bh_pval: number;
      pval_reactions: number;
      bonferroni_pval_reactions: number;
      bh_pval_reactions: number;
    };
  };
}
